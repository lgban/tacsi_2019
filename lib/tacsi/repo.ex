defmodule Tacsi.Repo do
  use Ecto.Repo,
    otp_app: :tacsi,
    adapter: Ecto.Adapters.Postgres
end
