defmodule Tacsi.Services.Geoloc do
    def distances_to(orgins, destinations) do
        origs = Enum.map(orgins, fn entry -> "#{entry[:latitude]},#{entry[:longitude]}" end) |> Enum.join(";")
        dests = Enum.map(destinations, fn entry -> "#{entry[:latitude]},#{entry[:longitude]}" end) |> Enum.join(";")
        res = HTTPoison.get "https://api.mapbox.com/directions-matrix/v1/mapbox/driving-traffic/#{origs};#{dests}?annotations=distance,duration&access_token=pk."
        
        IO.inspect res
    end
end