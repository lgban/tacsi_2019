defmodule Tacsi.Sales.Booking do
  use Ecto.Schema
  import Ecto.Changeset


  schema "sales_bookings" do
    field :dropoff_address, :string
    field :pickup_address, :string
    field :customer_id, :id

    timestamps()
  end

  @doc false
  def changeset(booking, attrs \\ %{}) do
    booking
    |> cast(attrs, [:pickup_address, :dropoff_address, :customer_id])
    |> validate_required([:pickup_address, :dropoff_address])
  end
end
