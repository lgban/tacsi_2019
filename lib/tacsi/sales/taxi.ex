defmodule Tacsi.Sales.Taxi do
  use Ecto.Schema
  import Ecto.Changeset


  schema "sales_taxis" do
    field :location, :string
    field :status, :string
    field :username, :string
    field :latitude, :float
    field :longitude, :float

    timestamps()
  end

  @doc false
  def changeset(taxi, attrs) do
    taxi
    |> cast(attrs, [:username, :location, :status, :latitude, :longitude])
    |> validate_required([:username, :location, :status])
  end
end
