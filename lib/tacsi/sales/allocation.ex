defmodule Tacsi.Sales.Allocation do
  use Ecto.Schema
  import Ecto.Changeset


  schema "sales_allocations" do
    field :status, :string
    field :customer_id, :id

    timestamps()
  end

  @doc false
  def changeset(allocation, attrs) do
    allocation
    |> cast(attrs, [:status])
    |> validate_required([:status])
  end
end
