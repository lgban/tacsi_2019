defmodule TacsiWeb.BookingController do
  use TacsiWeb, :controller

  import Ecto.Query, only: [from: 2]

  alias Tacsi.Repo
  alias Tacsi.Sales.{Booking, Taxi}


  def index(conn, _params) do
    user = conn.assigns.current_user
    IO.puts "voy a desplegar los bookings del usuario #{user.username}"
    

    # select *
    # from Booking
    # where customer_id = ?

    query = from b in Booking, where: b.customer_id == ^user.id

    bookings = Repo.all(query)
    IO.puts "============================="
    IO.inspect bookings
    render(conn, "index.html", bookings: bookings)
  end

  def new(conn, _params) do
    changeset = Booking.changeset(%Booking{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, params) do
    IO.inspect params
    
    query = from t in Taxi, where: t.status == "available", select: t

    # Repo.all(query)



    changeset = Booking.changeset(%Booking{}, params["booking"] |> Map.put("customer_id", 2))

    Repo.insert(changeset)
    IO.inspect changeset

    conn
    |> put_flash(:info, "Your taxi will arrive in 5 minutes!")
    |> redirect(to: Routes.booking_path(conn, :index))
  end
end
