defmodule TacsiWeb.PageController do
  use TacsiWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
