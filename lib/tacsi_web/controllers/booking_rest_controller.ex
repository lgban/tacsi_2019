defmodule TacsiWeb.BookingRESTController do
    use TacsiWeb, :controller
    
    def create(conn, params) do
        IO.inspect params
        # Composición de queries
        TacsiWeb.Jobs.RideAllocationJob.start_link( 
            params 
            |> Map.put("latitude", 19.0184472) 
            |> Map.put("longitude", -98.2444165)
            |> Map.put("customer_id", 1)
        )
        json(conn, %{msg: "We are processing your request"})
    end

    def handle_response(conn, %{"response" => "accept"} = params) do
        IO.puts "I am in"
        IO.inspect params

        atom = params["id"] |> String.to_atom
        
        # Obtener referencia a GenServer
        # job.handle_acceptance(params)

        GenServer.cast(atom, {:handle_acceptance, params})

        json(conn, %{msg: "Todo mundo esta bien"})
    end

    def handle_response(_conn, %{"response" => "reject"} = _params) do
        # Obtener referencia a GenServer
        # job.handle_rejection(params)
    end

end