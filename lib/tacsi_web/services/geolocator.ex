defmodule TacsiWeb.Services.Geolocator do
    def distances_to(origins, destination) do
        IO.puts "We are in"
        IO.inspect origins
        IO.inspect destination
        IO.puts "------+++++------"

        str =
            [%{latitude: destination["latitude"], longitude: destination["longitude"]}] ++ origins
            |> Enum.map(fn %{latitude: lat, longitude: long} -> "#{long},#{lat}" end)
            |> Enum.join(";")
        
        IO.puts "------+++++------"
        # IO.inspect str

        {:ok, res} = HTTPoison.get "https://api.mapbox.com/directions-matrix/v1/mapbox/driving-traffic/#{str}?destinations=0&annotations=distance,duration&access_token=pk.eyJ1IjoibGdiYW51ZWxvcyIsImEiOiJjanVlZm02ZmEwMWJ0M3lwZG43aGh5dHkwIn0.bkjOWEzJQMkfuoCv82x-Og"

        body = Poison.Parser.parse!(res.body)

        Enum.zip(body["durations"], Enum.map(origins, fn o -> o.username end))
        |> Enum.sort
    end
end