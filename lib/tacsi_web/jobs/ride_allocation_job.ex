defmodule TacsiWeb.Jobs.RideAllocationJob do
    use GenServer

    import Ecto.Query, only: [from: 2]

    alias Tacsi.Repo
    alias Tacsi.Sales.{Booking, Taxi}
    alias TacsiWeb.Services.Geolocator

    def start_link(request) do
        changeset = Booking.changeset(%Booking{}, request)

        changeset = Repo.insert!(changeset)
        IO.inspect changeset
    
        GenServer.start_link(__MODULE__, Map.put(request, "booking_id", changeset.id), name: changeset.id |> Integer.to_string() |> String.to_atom())
    end

    def init(request) do
        IO.puts "Entre a ride allocation job"
        IO.inspect request

        query = from t in Taxi, where: t.status == "available", select: t
        available = Repo.all(query)

        IO.puts "=============================== Available"
        IO.inspect available

        if (length(available) > 0) do
            candidates = Geolocator.distances_to(available, request)
            handle_search_driver(%{
                request: request, 
                candidates: candidates, 
                discarded_candidates: []
            })
        else
            {:error}
        end

        # Buscar a los taxistas candidatos
    end

    def handle_search_driver(state) do
        %{request: request, candidates: candidates, discarded_candidates: discarded} = state

        if (length(candidates) > 0) do
            # Seleccionar el más cercano
            {_duration, taxi_username} = taxi = List.first(candidates)

            IO.puts "Contacting #{taxi_username}"

            # Enviar petición al taxista mas cercano
            TacsiWeb.Endpoint.broadcast(
                "driver:" <> taxi_username,
                "request",
                request
            )

            timer = Process.send_after(self(), :driver_response_timeout, 30000)
        
            {:ok, %{
                request: request, 
                candidates: tl(candidates), 
                discarded_candidates: discarded, 
                current: taxi, 
                timeout: timer
            }}
        else
            {:error}
        end
    end

    def handle_rejection() do

    end

    def handle_cast(:handle_rejection, state) do
        %{timeout: timer} = state
        Process.cancel_timer(timer)
        handle_search_driver(state)
    end

    def handle_cast({:handle_acceptance, params}, state) do
        IO.puts "Entre a acceptance"
        IO.inspect params

        {:noreply, state}
    end

    def handle_info(:driver_response_timeout, state) do
        %{request: _request, candidates: _candidates, discarded_candidates: discarded, current: current} = state

        # Notificar a "current" que se tardo mucho y que contactamos a otro taxista
        IO.puts "Driver didn't reply"

        {:ok, new_state} = handle_search_driver(
            state 
            |> Map.put(:discarded_candidates, [discarded | current])
        )

        {:noreply, new_state}
    end
end


        # available = Repo.all(query)
        # if length(available) > 0 do
        #     taxi = List.first(available)

        #     Multi.new
        #     |> Multi.insert(:sales_bookings, Booking.changeset(%Booking{}, %{pickup_address: params["pickup_address"], dropoff_address: params["dropoff_address"]}))
        #     |> Multi.insert(:sales_allocations, Allocation.changeset(%Allocation{}, %{taxi_id: taxi.id, customer_id: 1, status: "allocated"}))
        #     |> Multi.update(:sales_taxis, Taxi.changeset(taxi, %{status: "busy"}))
        #     |> Repo.transaction


        #     IO.puts "=========================================="
        #     IO.puts "Done with broadcast"
        #     IO.puts "=========================================="

        #     conn
        #     |> json(%{msg: "Your taxi will arrive in 5 mins!"})
        # else
        #     conn
        #     |> json(%{msg: "Your taxi will arrive in 5 mins!"})    
        # end
