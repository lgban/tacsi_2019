defmodule TacsiWeb.Plugs.Authentication do
  import Plug.Conn

  def init(opts) do
    opts[:repo]
  end 

  def call(conn, repo) do
    IO.puts "Hi there ..."
    user = repo.get(Tacsi.Accounts.User, 2)
    IO.inspect user
    IO.inspect conn
    conn
    |> assign(:current_user, user)
    |> IO.inspect
  end
end