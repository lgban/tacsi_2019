defmodule TacsiWeb.Router do
  use TacsiWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug TacsiWeb.Plugs.Authentication, repo: Tacsi.Repo
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TacsiWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/users", UserController
    resources "/bookings", BookingController
  end

  # Other scopes may use custom stacks.
  scope "/api", TacsiWeb do
    pipe_through :api

    post "/bookings", BookingRESTController, :create
    patch "/bookings/:id", BookingRESTController, :handle_response
  end
end
