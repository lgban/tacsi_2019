defmodule TacsiWeb.BookingControllerTest do
  use TacsiWeb.ConnCase

  test "POST /bookings", %{conn: conn} do
    conn = post(conn, "/bookings", %{bookings: [pickup_address: "Tec", dropoff_address: "Plaza Dorada"]} )
    conn = get conn, redirected_to(conn)
    assert html_response(conn, 200) =~ "Your taxi will arrive in 5 minutes!"
  end
end
