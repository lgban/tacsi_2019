defmodule Tacsi.Repo.Migrations.CreateSalesAllocations do
  use Ecto.Migration

  def change do
    create table(:sales_allocations) do
      add :status, :string
      add :customer_id, references(:users, on_delete: :nothing)
      add :taxi_id, references(:sales_taxis, on_delete: :nothing)

      timestamps()
    end

    create index(:sales_allocations, [:customer_id, :taxi_id])
  end
end
