defmodule Tacsi.Repo.Migrations.AddLatitudeAndLongitudeToTaxi do
  use Ecto.Migration

  def change do
    alter table(:sales_taxis) do
      add :latitude, :float
      add :longitude, :float
    end   
  end
end
