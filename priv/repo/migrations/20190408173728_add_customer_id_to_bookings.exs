defmodule Tacsi.Repo.Migrations.AddCustomerIdToBookings do
  use Ecto.Migration

  def change do
    alter table(:sales_bookings) do
      add :customer_id, references(:users, on_delete: :nothing)
    end   
  end
end
