defmodule Tacsi.Repo.Migrations.CreateSalesTaxis do
  use Ecto.Migration

  def change do
    create table(:sales_taxis) do
      add :username, :string
      add :location, :string
      add :status, :string

      timestamps()
    end

  end
end
