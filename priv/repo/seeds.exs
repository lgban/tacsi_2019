# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Tacsi.Repo.insert!(%Tacsi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Tacsi.Accounts.User
alias Tacsi.Sales.Taxi
alias Tacsi.Repo

[%User{name: "Frodo Baggins", username: "frodo", password: "clave"},
%User{name: "Samwise Gamgee", username: "sam", password: "clave"},
%User{name: "Meriadoc Brandybuck", username: "merry", password: "clave"},
%User{name: "Peregrin Took", username: "pippin", password: "clave"},
%User{name: "Aragon", username: "strider", password: "clave"},]
|> Enum.map(fn user -> Repo.insert!(user) end)

[
    %Taxi{username: "merry", location: "La Noria", latitude: 19.0346939, longitude: -98.2260828, status: "available"},
    %Taxi{username: "sam", location: "Via San Angel", latitude: 19.0269748, longitude: -98.2400751, status: "available"},
    %Taxi{username: "pippin", location: "Angelopolis", latitude: 19.0319834, longitude: -98.2349368, status: "available"}]
|> Enum.map(fn taxi -> Repo.insert!(taxi) end)
