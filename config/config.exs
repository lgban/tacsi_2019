# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :tacsi,
  ecto_repos: [Tacsi.Repo]

# Configures the endpoint
config :tacsi, TacsiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "95hpXteS61SuljsRHm7aqTdC4wks62Vp4ZTKn4Prwp2QlzJwlded0sW3s3HJr/ew",
  render_errors: [view: TacsiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Tacsi.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
