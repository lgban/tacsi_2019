defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  alias Tacsi.{Repo, Sales.Taxi}
  
  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Repo, {:shared, self()})
    %{}
  end

  scenario_finalize fn _status, _state -> 
    Ecto.Adapters.SQL.Sandbox.checkin(Repo)
    # Hound.end_session
  end

  given_ ~r/^the following taxis are on duty$/, fn state, %{table_data: data} ->
    IO.inspect data
    data
    |> Enum.map(fn taxi -> Taxi.changeset(%Taxi{}, taxi) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want to go from "(?<pickup_addr>[^"]+)" to "(?<dropoff_addr>[^"]+)"$/,
  fn state, %{pickup_addr: pickup_addr, dropoff_addr: dropoff_addr} ->
    {:ok, Map.put(state, :pickup_addr, pickup_addr) |> Map.put(:dropoff_addr, dropoff_addr)}
  end

  and_ ~r/^I open STRS' web page$/, fn state ->
    IO.inspect state
    navigate_to "/bookings/new"
    {:ok, state}
  end

  and_ ~r/^I enter the booking information$/, fn state ->
    fill_field({:id, "pickup_address"}, state[:pickup_addr])
    fill_field({:id, "dropoff_address"}, state[:dropoff_addr])
    {:ok, state}
  end

  when_ ~r/^I summit the booking request$/, fn state ->
    click({:id, "submit_button"})
    {:ok, state}
  end

  then_ ~r/^I should receive a confirmation message$/, fn state ->
    assert visible_in_page? ~r/Your taxi will arrive in \d+ minutes/
    {:ok, state}
  end
end
